HOST = 'localhost'
PORT = 8080

# Link / to ./frontend ?
ENABLE_FRONTEND = True

# Which folder to store uploaded images to?
IMAGE_FOLDER = './images'

db = {
    'host': 'localhost',
    'user': 'postgres',
    'password': '',
}

redis = 'redis://localhost'

# 72 hours, 3 days.
TIMED_TOKEN_AGE = 259200

# run clamdscan on every upload.
# this will use the multicore option,
# so it is not recommended on low-end machines.
UPLOAD_SCAN = False

# When UPLOAD_SCAN is true and
# a file is detected as being malicious,
# we will call this discord webhook
# with data about the upload.
UPLOAD_SCAN_WEBHOOK = ""

# Webhook for banned users.
USER_BAN_WEBHOOK = ""

# Webhook for JPEGs that grew too much after EXIF cleaning.
EXIF_TOOBIG_WEBHOOK = ""

# change this to your wanted ban period
# '1 day', '6 hours', '10 seconds', etc.
BAN_PERIOD = '6 hours'

# How many ratelimit payloads can be sent
# before the current user gets banned?
RL_THRESHOLD = 10

# Should we clear EXIF values for JPEGs?
# Needs Pillow
CLEAR_EXIF = False

# To prevent potential exploits to take over a lot of storage space
# by abusing EXIF cleaning, we're comparing size of images before and after
# EXIF cleaning. If exif_cleaned_filesize/regular_filesize is bigger than
# the following number, we just use non-exif cleaned versions of the files.
# Using anything below 1.25 or so might cause false positives.
# Set to None or 0 to disable check.
EXIF_INCREASELIMIT = 2

ACCEPTED_MIMES = [
    'image/png',
    'image/jpeg',
    'image/gif',
    'image/webp',
    'image/svg+xml',
    'audio/webm',
    'video/webm'
]

RATELIMIT = {
    'requests': 5,
    'second': 3,
}

# Enable thumbnails?
THUMBNAILS = False
THUMBNAIL_FOLDER = './thumbnails'

# Thumbnail sizing
THUMBNAIL_SIZES = {
    # large
    'l': (5000, 5000),

    # medium
    'm': (1000, 1000),

    # small
    's': (500, 500),

    # tiny
    't': (250, 250),
}

